/******************************************************************************
 * (c) 2010-2020 AED Team
 * Revisions: v2.1 ACR / v3.0 AED Team Out 2013 / v4.0 AED Team, Nov 2019
 * Last mod: abl 2020-05-19
 *
 * DESCRIPTION
 *	Auxiliary functions for Tree lab
 *
 * COMMENTS
 *   Code for distribution
 ******************************************************************************/

#include <limits.h>
#include "tree.h"
#include "queue.h"
#define min(i,j) (((i) <= (j)) ? (i) : (j))
#define INFINITY 999999999


struct _Node {
    int value;
    int level;
    struct _Node *left, *right;
};




/******************************************************************************
 * AllocNode()
 *
 * Arguments: (none)
 * Returns: new node
 * Side-Effects: none
 *
 * Description: allocates space for a new node
 *
 *****************************************************************************/

Node *AllocNode() {
    return ((Node *) malloc(sizeof(Node)));
}


/******************************************************************************
 * NewNode()
 *
 * Arguments: integer
 * Returns: new node
 * Side-Effects: none
 *
 * Description: creates a new node with a integer value
 *
 *****************************************************************************/

Node *NewNode(int num) {
    Node *aux;
    
    if (num==-1)
        return NULL;
    
    aux = AllocNode();
    aux->value = num;
    aux->level = -1;
    aux->right = aux->left = NULL;
    
    return aux;
}


/******************************************************************************
 * Construct()
 *
 * Arguments: input file and filename
 * Returns: pointer to root node of the tree
 * Side-Effects: none
 *
 * Description: constructs a tree reading integer numbers from file
 *
 *****************************************************************************/

Node *Construct(FILE *fp, char * filename) {
    Node *nodeRoot;
    int num;
    
    if (fscanf(fp,"%d", &num) == EOF)  {
        fprintf(stderr,
                "File %s has an insufficient number of values.  Please correct.\n",
                filename);
        exit(0);
    }
    
    if ((nodeRoot = NewNode(num)) != NULL) {
        /* links to child */
        nodeRoot->left  = Construct(fp, filename);
        nodeRoot->right = Construct(fp, filename);
    }
    
    return nodeRoot;
}


/******************************************************************************
 * FreeTree()
 *
 * Arguments: node - a tree root
 *
 * Returns: void
 * Side-Effects: none
 *
 * Description: frees the tree
 *
 *****************************************************************************/

void FreeTree(Node * root) {
    if (root != NULL) {
        FreeTree(root->left);
        FreeTree(root->right);
        free(root);
    }
}


/******************************************************************************
 * spaces()
 *
 * Arguments: n - number of spaces
 * Returns: void
 * Side-Effects: none
 *
 * Description: prints n spaces
 *
 *****************************************************************************/

void spaces(int n) {
    int i;
    for (i = 0; i < n; i++)
    printf("  ");
    return;
}


/******************************************************************************
 * PreFixed()
 *
 * Arguments: root - root of the tree
 *	      n - height of the tree
 * Returns: void
 * Side-Effects: none
 *
 * Description: prints the tree in the PreFix format
 *
 *****************************************************************************/

void PreFixed(Node *root, int n) {
    if (root != NULL) {
        spaces(n);
        printf("%d\n", root->value);
        PreFixed(root->left, n + 1);
        PreFixed(root->right, n + 1);
    }
    return;
}


/******************************************************************************
 * InFixed()
 *
 * Arguments: root - root of the tree
 *	      n - height of the tree
 * Returns: void
 * Side-Effects: none
 *
 * Description: prints the tree in the InFix format
 *
 *****************************************************************************/

void InFixed(Node *root, int n) {
    /* Insert code */
    if (root == NULL) {
        return;
    }
    InFixed(root->left, n+1);
    spaces(n);
    printf("%d\n", root->value);
    InFixed(root->right, n+1);
    return;
}


/******************************************************************************
 * PosFixed()
 *
 * Arguments: root - root of the tree
 *	      n - height of the tree
 * Returns: void
 * Side-Effects: none
 *
 * Description: prints the tree in the PosFix format
 *
 *****************************************************************************/

void PosFixed(Node *root, int n) {
    /* Insert code */
    if (root == NULL) {
        return;
    }
    PosFixed(root->left, n+1);
    PosFixed(root->right, n+1);
    spaces(n);
    printf("%d\n", root->value);
    return;
}


/******************************************************************************
 * sweepDepth()
 *
 * Arguments: root - root of the tree
 *	      n - height of the tree
 * Returns: void
 * Side-Effects: none
 *
 * Description: prints the tree in the depthfirst format
 *
 *****************************************************************************/

void sweepDepth(Node *root, int n) {
    /* Insert code */
    if(root == NULL){
        return;
    }
    spaces(n);
    printf("%d\n", root->value);
    sweepDepth(root->left, n+1);
    sweepDepth(root->right, n+1);
    
    return;
}


void addLevel(Node *root, int n){
    if (root == NULL) {
        return;
    }
    root->level = n;
    addLevel(root->left, n+1);
    addLevel(root->right, n+1);
}

/******************************************************************************
 * sweepBreadth()
 *
 * Arguments: root - root of the tree
 *
 * Returns: void
 * Side-Effects: none
 *
 * Description: prints the tree in the Breathfirst format
 *
 *****************************************************************************/

void sweepBreadth(Node *root) {
    /* Insert code */
    Queue * q = QueueNew();
    Node * aux = root;
    addLevel(root, 0);
    InsertLast(q, (Item *) aux);
    aux = (Node *) GetFirst(q);
    while (aux != NULL) {
        InsertLast(q, (Item *) aux->left);
        InsertLast(q, (Item *) aux->right);

        if (aux != NULL){
            spaces(aux->level);
            printf("%d\n", aux->value);
        }
        aux = (Node *) GetFirst(q);
    }
    
    return;
}

int GoDownCheckOrder(Node * root){
    int min = 0;
    if (root->left == NULL && root->right == NULL) {
        return root->value;
    }
    if (root->left != NULL) {
        min = GoDownCheckOrder(root->left);
    } else {
        return root->value;
    }
    if (root->right == NULL) {
        if (root->value > min) {
            return min;
        }
    }
    if (GoDownCheckOrder(root->right) > min && root->value > min) {
        return min;
    } else {
        return -1;
    }
}

/******************************************************************************
 * IsTreeOrdered()
 *
 * Arguments: node - a tree root
 *
 *
 * Returns: Boolean (TRUE if the tree is ordered, FALSE otherwise)
 * Side-Effects: none
 *
 * Description: checks if a tree is ordered
 *
 *****************************************************************************/

Boolean isTreeOrdered(Node * root) {
    /* Insert code */
    /*
    if (root->left != NULL) {
        if (root->left->value > root->value){
            return FALSE;
        } else {
            return isTreeOrdered(root->left);
        }
    }
    if (root->right != NULL) {
        if (root->value > root->right->value){
            return FALSE;
        } else {
            return isTreeOrdered(root->left);
        }
    }
    */
    if (GoDownCheckOrder(root) == -1) {
        return FALSE;
    }
    return TRUE;
}

int height(Node * root){
    int hl;
    int hr;
    if (root == NULL) {
        return 0;
    }
    if (root->left == NULL && root->right == NULL) {
        return 1;
    }
    
    hl = height(root->left);
    hr = height(root->right);
    return ((hl>hr) ? 1+hl : 1+hr);
}
/******************************************************************************
 * IsTreeBalanced()
 *
 * Arguments: node - a tree root
 *
 *
 * Returns: Boolean (TRUE if the tree is AVL balanced, FALSE otherwise)
 * Side-Effects: none
 *
 * Description: checks if a tree is AVL balanced
 *
 *****************************************************************************/

Boolean isTreeBalanced(Node * root) {
    /* Insert code */
    if (root == NULL) {
        return TRUE;
    }
    if ((abs(height(root->left)-height(root->right)) <= 1) && isTreeBalanced(root->left) && isTreeBalanced(root->right)) {
        return TRUE;
    }
    return FALSE;
}

int FindDepth(Node *root, int n){
    int l, r;
    l = INFINITY;
    r = INFINITY;
    if (root->left == NULL && root->right == NULL) {
        return n;
    }
    if (root->right != NULL) {
        r = FindDepth(root->right, n+1);
    }
    if (root->left) {
        l = FindDepth(root->left, n+1);
    }
    return min(l, r);
}

void Traverse(Node *root, int n, int go_to){
    if(n+1 == go_to){
        if (root->left != NULL) {
            if (root->left->left == NULL && root->left->right == NULL) {
                printf("Rem %d\n", root->left->value);
                free(root->left);
                root->left = NULL;
            }
        }
        if (root->right != NULL) {
            if (root->right->left == NULL && root->right->right == NULL) {
                printf("Rem %d\n", root->right->value);
                free(root->right);
                root->right = NULL;
            }
        }
        return;
    }
    
    Traverse(root->left, n+1, go_to);
    Traverse(root->right, n+1, go_to);
}

void RemLeastDeep(Node * root){
    int d;
    d = FindDepth(root, 0);
    Traverse(root, 0, d);
}
