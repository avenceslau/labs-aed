#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(int argc, char **argv)
{
  int a, b, k, p;

  printf("Escreva os valores das constantes no seguinte formato: C_N = aC_(N/b) + O(N^klog^p(N))\n");
  printf("a = ");
  scanf("%d", &a);
  printf("b = ");
  scanf("%d", &b);
  printf("k = ");
  scanf("%d", &k);
  printf("p = ");
  scanf("%d", &p);

  b = (double) b;

  if (a > pow(b, k))
    printf("C_N = O(N^(log_(%d)(%d)))\n", b, a);
  else if (a == pow(b, k))
    if (p > -1)
      printf("C_N = O(N^(log_(%d)(%d))log^(%d)(N))\n", b, a, p + 1);
    else if (p == -1)
      printf("C_N = O(N^(log_(%d)(%d))log(log(N)))\n", b, a);
    else 
      printf("C_N = O(N^(log_(%d)(%d)))\n", b, a);
  else
    if (p >= 0)
      printf("C_N = O(N^(%d)log^(%d)(N))\n", k, p);
    else
      printf("C_N = O(N^(%d))\n", k);

  exit(0);
}