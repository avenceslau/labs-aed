/******************************************************************************
* (c) 2010-2019 AED Team
* Last modified: abl 2019-02-22
*
* NAME
*   connectivity.c
*
* DESCRIPTION
*   Algorithms for solving the connectivity problem -  QF QU WQU CWQU
*   For each method count number of entry pairs and the number of links
*
* COMMENTS
*   Code for public distribution
******************************************************************************/
#include<stdio.h>
#include<stdlib.h>

#include "connectivity.h"


typedef struct num_list num_node;
typedef struct quniondata roots;

struct num_list{
    int num;
    int id;  //??????
//    num_node * next_num;
};

struct quniondata{
    int num;
    int id;
    roots *next;
};

//void AddNode(num_node **head, num_node **tail){
//    num_node *new_node = NULL;
//
//    new_node = (num_node *) malloc(sizeof(new_node));
//    if (*head == NULL) {
//        *head = new_node;
//        new_node->next_num = NULL;
//        *tail = *head;
//    } else {
//        (*tail)->next_num = new_node;
//        new_node->next_num = NULL;
//        *tail = new_node;
//    }
//}

int QuickFindPrint(int N, num_node * list_id){
    int i, j, num_conjuntos = 0;
    
    for (i = 0 ; i < N; i++) {
        if (list_id[i].id != -1 && i == list_id[i].num) {
            num_conjuntos++;
            printf("%d", list_id[i].num);
            for (j = 1; j < N; j++) {
                if (list_id[i].id == list_id[j].id && list_id[j].num !=0) {
                    printf("-%d", list_id[j].num);
                    list_id[j].id = -1;
                }
            }
            list_id[i].id = -1;
            printf("\n");
        }
    }
    return num_conjuntos;
}

int checkmax (int a, int b){
    int c;
    if (a > b) {
        c = a;
    } else {
        c = b;
    }
    return c;
}

/******************************************************************************
* quick_find()
*
* Arguments: id - array with connectivity information
*            N - size of array
*            fp - file pointer to read data from
*            quietOut - to reduce output to just final count
* Returns: (void)
* Side-Effects: pairs of elements are read and the connectivity array is
*               modified
*
* Description: Quick Find algorithm
*****************************************************************************/
void quick_find(int *id, int N, FILE * fp, int quietOut)
{
    int i, p, q, t;
    int pairs_cnt = 0;            /* connection pairs counter */
    int links_cnt = 0;            /* number of links counter */
    int num_conjuntos = 0;
    unsigned long int find_ctr = 0;
    unsigned long int union_ctr = 0;
//    num_node * list_id = NULL;

//    list_id = (num_node *) calloc(N, sizeof(num_node));

    /* initialize; all disconnected */
    for (i = 0; i < N; i++) {
        id[i] = i;
//        list_id[i].id = i;
    }


    /* read while there is data */
    while (fscanf(fp, "%d %d", &p, &q) == 2) {
        pairs_cnt++;
//        list_id[p].num = p;
//        list_id[q].num = q;
        /* do search first */
        find_ctr += 2;
        if (id[p] == id[q]) {
            /* already in the same set; discard */
            if (!quietOut)
                printf("\t%d %d\n", p, q);
            continue;
        }

        /* pair has new info; must perform union */
        union_ctr++;
        for (t = id[p], i = 0; i < N; i++) {
            union_ctr++;
            if (id[i] == t) {
                union_ctr+=2;
                id[i] = id[q];
//                list_id[i].id = list_id[q].id;
            }
        }
        links_cnt++;
        if (!quietOut)
            printf(" %d %d\n", p, q);
    }
    printf("QF: The number of links performed is %d for %d input pairs.\n", links_cnt, pairs_cnt);
    printf("Número de operações de find: %lu\nNúmero de operações de union: %lu\n", find_ctr, union_ctr);
    
    
    for (i = 0; i < N; i++) {
        if (id[i] < (N+1) && id[i] == i) {
            num_conjuntos++;
            printf("%d", i);
            for (t = 0; t < N; t++) {
                if (id[i] == id[t] && t != i) {
                    printf("-%d", t);
                    id[t] += (N+1);
                }
            }
            id[i] += (N+1);
            printf("\n");
        }
    }
    for (i = 0; i < N; i++) {
        id[i] = id[i] - (N+1);
    }
    
    printf("\n");
    printf("Número de conjuntos: %d\n\n", num_conjuntos);
}


//void ListUnion(roots * list, int i, int j){
//    list[i].next = list[j].next;
//    list[j].next = &list[i];
//}
//
//void QuickUnionPrint(roots * num, int N){
//    int i;
//    roots * to_print;
//    for (i = 0; i < N; i++) {
//        if (num[i].id == num[i].num) {
//            printf("%d",num[i].num);
//            to_print = num[i].next;
//            while (to_print != NULL) {
//                printf("-%d", to_print->id);
//                to_print = to_print->next;
//            }
//            printf("\n Teste \n");
//        }
//    }
//}

/******************************************************************************
* quick_union()
*
* Arguments: id - array with connectivity information
*            N - size of array
*            fp - file pointer to read data from
*            quietOut - to reduce output to just final count
* Returns: (void)
* Side-Effects: pairs of elements are read and the connectivity array is
*               modified
*
* Description: Quick Union algorithm
*****************************************************************************/
void quick_union(int *id, int N, FILE * fp, int quietOut) {
    int i, j, p, q;
    int pairs_cnt = 0;            /* connection pairs counter */
    int links_cnt = 0;            /* number of links counter */
    unsigned long int find_ctr = 0;
    unsigned long int union_ctr = 0;
    roots * list = (roots *) calloc(N, sizeof(roots));
    /* initialize; all disconnected */
    for (i = 0; i < N; i++) {
        id[i] = i;
        list[i].id = i;
        list[i].next = NULL;
    }

    /* read while there is data */
    while (fscanf(fp, "%d %d", &p, &q) == 2) {
        pairs_cnt++;
        i = p;
        j = q;
        list[p].num = p;
        list[q].num = q;

        /* do search first */
        while (i != id[i]) {
            find_ctr+=2; //ciclo + op
            i = id[i];
        }
        find_ctr++; //ciclo final
        
        while (j != id[j]) {
            find_ctr+=2;//ciclo + op
            j = id[j];
        }
        find_ctr++; //ciclo final
        if (i == j) {
            /* already in the same set; discard */
            if (!quietOut)
                printf("\t%d %d\n", p, q);
            continue;
        }

        /* pair has new info; must perform union */
        id[i] = j;
        list[i].id = j;
//        ListUnion(list, i, j);
        union_ctr++; //acesso id i
        links_cnt++;

        if (!quietOut)
            printf(" %d %d\n", p, q);
    }
//    for (i = 0; i < N; i++) {
//        printf("%d-", id[i]);
//    }
    printf("\n\n");
    printf("QU: The number of links performed is %d for %d input pairs.\n",
            links_cnt, pairs_cnt);
    printf("Número de operações de find: %lu\nNúmero de operações de union: %lu\n", find_ctr, union_ctr);
    
//    QuickUnionPrint(list, N);
}




/******************************************************************************
* weighted_quick_union()
*
* Arguments: id - array with connectivity information
*            N - size of array
*            fp - file pointer to read data from
*            quietOut - to reduce output to just final count
* Returns: (void)
* Side-Effects: pairs of elements are read and the connectivity array is
*               modified
*
* Description: Weighted Quick Union algorithm
*****************************************************************************/
void weighted_quick_union(int *id, int N, FILE * fp, int quietOut){
    int i, j, p, q;
    int *sz = (int *) malloc(N * sizeof(int));
    int pairs_cnt = 0;            /* connection pairs counter */
    int links_cnt = 0;            /* number of links counter */
    unsigned long int find_ctr = 0;
    unsigned long int union_ctr = 0;

    /* initialize; all disconnected */
    for (i = 0; i < N; i++) {
        id[i] = i;
        sz[i] = 1;
    }

    /* read while there is data */
    while (fscanf(fp, "%d %d", &p, &q) == 2) {
        pairs_cnt++;
        
        /* do search first */
        for (i = p; i != id[i]; i = id[i], find_ctr+=2); //id[i]*2 check out e incrementação
        for (j = q; j != id[j]; j = id[j], find_ctr+=2); //id[j]*2 check out e incrementação
        
        if (i == j) {
            /* already in the same set; discard */
            if (!quietOut)
                printf("\t%d %d\n", p, q);
            continue;
        }
        
        /* pair has new info; must perform union; pick right direction */
        union_ctr+=2; //comparação
        if (sz[i] < sz[j]) {
            id[i] = j;
            sz[j] += sz[i];
            union_ctr+=3; //id i e sz*2
        }
        else {
            id[j] = i;
            sz[i] += sz[j];
            union_ctr+=3; // same as above
        }
        links_cnt++;
        
        if (!quietOut)
            printf(" %d %d\n", p, q);
    }
    printf("WQU: The number of links performed is %d for %d input pairs.\n",
            links_cnt, pairs_cnt);
    printf("Número de operações de find: %lu\nNúmero de operações de union: %lu\n", find_ctr, union_ctr);
}


/******************************************************************************
* compressed_weighted_quick_union()
*
* Arguments: id - array with connectivity information
*            N - size of array
*            fp - file pointer to read data from
*            quietOut - to reduce output to just final count
* Returns: (void)
* Side-Effects: pairs of elements are read and the connectivity array is
*               modified
*
* Description: Compressed Weighted Quick Union algorithm
*****************************************************************************/
void compressed_weighted_quick_union(int *id, int N, FILE * fp, int quietOut){
    int i, j, p, q, t, x;
    int *sz = (int *) malloc(N * sizeof(int));
    int pairs_cnt = 0;            /* connection pairs counter */
    int links_cnt = 0;            /* number of links counter */
    unsigned long int find_ctr = 0;
    unsigned long int union_ctr = 0;

    /* initialize; all disconnected */
    for (i = 0; i < N; i++) {
        id[i] = i;
        sz[i] = 1;
    }

    /* read while there is data */
    while (fscanf(fp, "%d %d", &p, &q) == 2) {
        pairs_cnt++;

        /* do search first */
        for (i = p; i != id[i]; i = id[i], find_ctr+=2); // idi *2
        for (j = q; j != id[j]; j = id[j], find_ctr+=2); // idj *2
        

        if (i == j) {
            /* already in the same set; discard */
            if (!quietOut)
                printf("\t%d %d\n", p, q);
            continue;
        }

        /* pair has new info; must perform union; pick right direction */
        union_ctr+=2;
        if (sz[i] < sz[j]) {
            id[i] = j;
            sz[j] += sz[i];
            t = j;
            union_ctr+=3;
        }
        else {
            id[j] = i;
            sz[i] += sz[j];
            t = i;
            union_ctr+=3;
        }
        links_cnt++;

        /* retrace the path and compress to the top */
        for (i = p; i != id[i]; i = x, union_ctr++) {
            x = id[i];
            id[i] = t;
            union_ctr+=2;
        }
        for (j = q; j != id[j]; j = x, union_ctr++) {
            x = id[j];
            id[j] = t;
            union_ctr+=2;
        }
        if (!quietOut)
            printf(" %d %d\n", p, q);
    }
    printf("CWQU: The number of links performed is %d for %d input pairs.\n",
            links_cnt, pairs_cnt);
    printf("Número de operações de find: %lu\nNúmero de operações de union: %lu\n", find_ctr, union_ctr);

    return;
}
            		
