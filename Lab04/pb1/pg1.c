//
//  main.c
//  Lab04
//
//  Created by André Venceslau on 17/11/2020.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct graphm GraphM;
struct graphm {int V; int E; int **adj;}; //V = Vertex, E = total edges
typedef struct edge Edge;
struct edge {int v; int w;};
Edge *EDGE(int, int);

double Density (int vertex, int n_edges){
    return (n_edges)/vertex;
}

int FindGrau(GraphM *G){
    int max_grau = 0;
    for (int i = 0; i < G->V ; i++) {
        int counter = 0;
        for (int j = 0; j < G->V ; j++) {
            if (G->adj[i][j] != 0) {
                counter++;
                if (counter > max_grau) {
                    max_grau = counter;
                }
            }
        }
    }
    return max_grau;
}

void PrintEdges(GraphM *G, FILE *fpOut){
    fprintf(fpOut, "%d %d\n", G->V, (G->E/2));
    for (int i = 0; i < (G->V)-1; i++) {
        for (int j = i+1; j < G->V; j++) {
            if (G->adj[i][j] != 0){
                fprintf(fpOut, "%d %d %d\n", i, j, G->adj[i][j]);
            }
        }
    }
}

void MATRIXinit(GraphM ** G){
    for (int i = 0 ; i < ((*G)->V) -1; i++) {
        for (int j = i+1; j < (*G)->V; j++) {
            (*G)->adj[i][j] = 0;
        }
    }
}

void InsertEdgeAdjMatrix(GraphM **G, Edge *e, int cost) {
    int v = e->v, w = e->w;
   (*G)->adj[v][w] = cost;
     if ((*G)->adj[v][w] == 0) (*G)->E++;
   

}

GraphM *GRAPHinit(int V) {
    int v;
    GraphM *G = (GraphM*) malloc(sizeof(GraphM));
    G->V = V;
    G->E = 0;
    G->adj = (int **) malloc(V * sizeof(int*));
    for (v = 0; v < V; v++)
        G->adj[v] = (int *) malloc(V*sizeof(int));
    return G;
}

bool FindTriangle(GraphM * G, int vertex){
    int * array;
    int n_links = 0;
    for (int i = 0; i < G->V ; i++) {
        if( G->adj[vertex][i] != 0){
            n_links++;
        }
    }
    array = (int *) malloc(sizeof(int)*n_links);
    int t = 0;
    for (int i = 0; i < G->V; i++) {
        if (G->adj[vertex][i] != 0) {
            array[t] = i;
            t++;
        }
    }
    
    for (int i = 0 ; i < t -1; i++) {
        for (int j = i; j < t; j++) {
            if (G->adj[array[i]][array[j]] != 0){
                return true;
            }
        }
    }
    
    return false;
}



int main(int argc, const char * argv[]) {
    // insert code here...
    FILE *fp;
    char * ch_v = strdup(argv[2]);
    int x = atoi(ch_v);
    char * filename = strdup(argv[1]);
    char *outname = strstr(argv[1], ".adj");
    GraphM *G;
    Edge e;
    e.v=0; e.w=0;
    int cost = 0, n;
    printf("%s\n", argv[2]);
    
    if (outname == NULL) {printf("invalid file type"); exit (0);}
    
    fp = fopen(filename, "r");
    
    fscanf(fp, "%d", &n);
    G = GRAPHinit(n);
    MATRIXinit(&G);
    
    for (int i = 0; i < G->V; i++) {
        for (int j = 0 ; j < G->V; j++) {
            e.v = i;
            e.w = j;
            fscanf(fp, "%d", &cost);
            InsertEdgeAdjMatrix(&G, &e,  cost);
        }
    }
 
    
 
    printf("Densidade %lf\n", Density(G->V, G->E));
    printf("Grau: %d\n", FindGrau(G));
    fclose(fp);
    *outname = '\0';
    fp = fopen(strcat(filename,".edges"), "w");
    
    PrintEdges(G, fp);
    
    fclose(fp);
    
    if (FindTriangle(G, x) == true) {
        printf("Has click\n");
    } else {
        printf("No click.\n");
    }
    
    for (int i = 0; i < G->V; i++) {
        free(G->adj[i]);
    }
    free(G->adj);
    return 0;
}

