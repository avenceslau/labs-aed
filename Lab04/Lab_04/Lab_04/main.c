//
//  main.c
//  Lab04
//
//  Created by André Venceslau on 17/11/2020.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LinkedList.h"

typedef struct graphl GraphL;
typedef struct node link;
typedef struct {
    int name;
    int weight;
} edge;
typedef struct {
    int grau;
    LinkedList * adj;
} vertex;
struct graphl {int V; int E; vertex *adj;};


int compare(Item a, Item b){
    return (((edge *) a)->name - ((edge* )b)->name);
}


GraphL * GetData(char * filename){
    GraphL * G = NULL;
    int n_vertex, n_links, err;
    FILE * fp = fopen(filename, "r");
    if (fp == NULL) {return NULL;}
    
    
    fscanf(fp, "%d %d", &n_vertex, &n_links);
    G = (GraphL*)malloc(sizeof(GraphL));
    G->adj = (vertex*) calloc(n_vertex, sizeof(vertex));
    for (int i = 0; i < n_links; i++) {
        int from, to, cost;
        fscanf(fp, "%d %d %d", &from, &to, &cost);
        edge * edge1 = (edge *) malloc(sizeof(edge));
        edge1->name = to;
        edge1->weight = cost;

        edge * edge2 = (edge *) malloc(sizeof(edge));
        edge2->name = from;
        edge2->weight = cost;

        vertex * vfrom = &G->adj[from];
        vertex * vto = &G->adj[to];
        
        vfrom->adj = insertSortedLinkedList(vfrom->adj, (Item) edge1, compare, &err);
        vto->adj = insertSortedLinkedList(vto->adj, (Item) edge2, compare, &err);

        
        vfrom->grau++;
        vto->grau++;
    }
    G->V = n_vertex;
    G->E = n_links;
    
    fclose(fp);
    return G;
}


void WriteOutFile(GraphL* graph, char* filename) {

    char* outFilename = (char*)malloc((strlen(filename) + 1) * sizeof(char));
    strcpy(outFilename, filename);
    char* ext = strrchr(outFilename, '.');
    strcpy(ext + 1, "ladj");

    FILE* fout = fopen(outFilename, "wt");
    if (fout == NULL)
        return;

    int n = graph->V;
    fprintf(fout, "%d\n", n);
    for (int i = 0; i < n; i++) {
        for (LinkedList* root = graph->adj[i].adj; root != NULL; root = getNextNodeLinkedList(root)) {
            edge* _edge = getItemLinkedList(root);
            fprintf(fout, "%d:%d ", _edge->name, _edge->weight);
            printf("%d:%d ", _edge->name, _edge->weight);
        }
        fprintf(fout, "%d\n", -1);
        printf("-1\n");

    }

    free(outFilename);
    fclose(fout);

}

void FreeEdge(Item item) {

    edge* edgex = (edge*)item;
    free(edgex);
}

void FreeGraph(GraphL* graph) {

    for (int i = 0; i < graph->V; i++)
        freeLinkedList(graph->adj[i].adj, FreeEdge);

    free(graph->adj);
    free(graph);
}

int main(int argc, char * argv[]){
    char * filename = argv[1];
    char *checkext = strrchr(filename, '.');
    if (strcmp(checkext,".edges")) {
        printf("wrong file type");
    }
    GraphL * G;
    G = GetData(filename);
    WriteOutFile(G, filename);
    
    FreeGraph(G);
    
    return 0;
}
